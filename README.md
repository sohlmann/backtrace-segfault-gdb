# Get a backtrace from a segfaulting MPI program

This example uses a non-interactive gdb script to generate a backtrace on the
rank that encounters the segfault. The output from each rank is redirected to a
different file.

## Usage

1. Load modules, e.g. `module load gcc/12 openmpi/4`
2. Compile code (`make`)
3. Get allocation with salloc (`make salloc`)
4. Run code interactively with srun (`make run`)

This technique can of course be also run in batch jobs. Using an allocation
allows for a more interactive workflow.

The gdb script can be extended with other gdb commands, e.g., to print out more
information on local variables.
