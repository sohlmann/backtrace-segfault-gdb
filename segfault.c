#include <mpi.h>

void segfault_function() {
  int *a = 0;
  *a = 0;
}

int main(int argc, char** argv) {
  int rank;

  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (rank == 0) segfault_function();

  MPI_Barrier(MPI_COMM_WORLD);

  MPI_Finalize();
  return 0;
}
