# needs module load gcc/12 openmpi/4
CC = mpicc
CFLAGS ?= -g

segfault: segfault.o

run: segfault
	srun gdb -q -x gdb-script segfault
clean:
	rm -f segfault segfault.o log*
salloc:
	salloc -N 2 -n 144 -c 1 --time=0:10:00
findtrace:
	grep SIGSEGV log*

.PHONY: run clean salloc findtrace
